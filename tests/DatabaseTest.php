<?php

require_once(dirname(__FILE__) . '/../src/Cuckoo/Database/Connect.php');

/**
 * MySQL mock driver
 */
class MySQL
{
	public function connect()
	{}
}

class DatabaseTest extends PHPUnit_Framework_TestCase
{
	protected $object = null;

	protected $config = array(
		'hostname'	=> 'localhost',
		'username'	=> 'myuser',
		'password'	=> 'mypass',
		'database'	=> 'mydb'
	);

	protected $driverMock;

	public function setUp()
	{
		$mock = $this->getMock('MySQL');
		$mock->expects($this->any())
			->method('connect')
			->will($this->returnValue(new PDO('mysql:dbname=mydb;host=localhost', 'myuser', 'mypass')));
		$this->object = new Cuckoo\Database\Connect($this->config, $mock);
	}

	public function tearDown()
	{
		$this->obj = null;
	}

	public function testConnectClassBasics()
	{
		// Test if we have access to Connect::$pdo and that it's an instance of PDO
		$this->assertInstanceOf('PDO', $this->object->pdo);

		// Test if the output of Connect::checkConfig() is a boolean
		$this->assertTrue(is_bool($this->object->checkConfig($this->config)));
	}

	/**
	 * Test custom query method with bound values
	 */
	public function testCustomQueryMethodWithBoundValues()
	{
		$sth = $this->object->query('SELECT * FROM users WHERE id=:id', array('id' => 1));
		$this->assertTrue(is_array($sth->fetch(PDO::FETCH_ASSOC)));
	}

	/**
	 * Test using PDO directly through the public Connect::$pdo property
	 */
	public function testCanUsePdoDirectly()
	{
		// Prepare and execute query
		$sth = $this->object->pdo->prepare('SELECT * FROM users WHERE id=:id');
		$sth->execute(array('id' => 1));

		$this->assertTrue(is_array($sth->fetch(PDO::FETCH_ASSOC)));
	}
}