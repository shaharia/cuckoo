Educational document
======

Commit [73523f1b5966d660d6cf246aece6ff614f5154f5](https://bitbucket.org/oaass/cuckoo/commits/73523f1b5966d660d6cf246aece6ff614f5154f5)
------

I decided to make the Database::connect() static so that connecting to a database would be
only one line of code (if you do not count the $config array).

```php
$config = array(
    'hostname'  => 'localhost',
    'username'  => 'user',
    'password'  => 'pass',
    'database'  => 'mydb',
    'driver'    => 'mysql'
);
$db = \Cuckoo\Databas\Database::connect($config);
```

When calling the static factory class what happens is that it looks for the driver in the
folder called `Drivers`. All drivers are static as well with only one method; `connect`.

What the connect() method does is that it connects to the database using PDO. It creates
the `dsn` string and it might also run additional commands based on configurations.

One example for an additional command would be when calling the MySQL driver, and the
`$config['charset']` is set. This will make the driver do the following before returning
the PDO instance

```php
if (array_key_exists('charset', $config)) {
    $pdo->query("SET NAMES '{$config['charset']}'");
}
```

This will return an instance of `Cuckoo\Database\Connection` which is the actuall class
which holds the database connection link.

Peer-review [73523f1b5966d660d6cf246aece6ff614f5154f5](https://bitbucket.org/oaass/cuckoo/commits/73523f1b5966d660d6cf246aece6ff614f5154f5)
-----------

Your aim as a software architect should not be to make the code needed as short
as possible, but to make the software reusable. As a consequence, "so that
connecting [...] only one line" is not a valid argument.

`static` is bad because it breaks the *object-orientedness* of the code. Why is
that? In order to understand this, we need to understand first what the actual
meaning of object-oriented programming is.

A class is like a blueprint, like a building plan for objects. Imagine you want
to build a house, and you go to an architect which makes for you a building
plan for the house. He then prints out that plan on paper, and gives this to
you. With this plan in your hand, you can now build as many identical houses as
you like.

This building plan is called a **class** in OOP.

When you then write `new` in your code, you create an object of that class -
a house, an actual 3D object you can live in.

When you put `static` in your code, you are attaching the static property or
method to the building plan, to the class, and not to the objects which would
be instantiated by `new`. So `static` breaks the object-orientedness, the very
core of object-oriented programming.

What happens for instance, if you want to allow the caller to change at runtime
the `Database` connection? Think about the case when a caller wants to subclass
your base class `Database`, for whatever reason, and wants to use that instead?
He would have to duplicate code.

Another problem is `Connection` class which is hard coded in the `Database`.
What if the user (the caller) want to change that with his own implementation?

And another smaller problem is an **implementation detail**: too much
repetitive code in `Database.php`, lines 38-55, regarding parameter validation.

Commit [f667b2ffa81302a6e9d97f62c3fbbfc054d14582](https://bitbucket.org/oaass/cuckoo/commits/f667b2ffa81302a6e9d97f62c3fbbfc054d14582)
------

Based on the peer-review I decided to get rid of the whole factory pattern since
it's not really needed. I also removed all static code, and renamed `Connection`
to `Connect`. The package is now much more dynamic when it comes to how it handles
drivers.

The driver is no longer a part of the `$config` array, but is instead injected
as an object directly into `Cuckoo\Database\Connect::__construct()`.

Even though I got rid of the factory pattern, the code to instantiate a new database
connection is just one line, again not considering the configuration array.

```php
$config = array(
    'hostname'  => 'localhost',
    'username'  => 'user',
    'password'  => 'pass',
    'database'  => 'mydb'
);
$db = new Cuckoo\Database\Connect($config, new Cuckoo\Database\Drivers\MySQL());
```

Peer-review [f667b2ffa81302a6e9d97f62c3fbbfc054d14582](https://bitbucket.org/oaass/cuckoo/commits/f667b2ffa81302a6e9d97f62c3fbbfc054d14582)
------

1. A class name should generally represent a thing, not an activity/action.
   Consequently, `Connect` is a bad name, because it stands for an action.

   Indeed, there are cases when it really makes sense to name classes after
   verbs, for instance if you are modelling the *command pattern* or perhaps
   some other *behavioral patterns* (read on wikipedia about them, and this
   paragraph will make sense).

   A good way to avoid these kind of confusions is to actually write out (in
   plain language) in the class level docstring what the class is for. It looks
   like an irrelevant step, but the rationale is based on psychology: when you
   think something, you don't actually really grasp what you're thinking,
   until you write it down or verbalize it in some way.

   Has it ever occured to
   you that you had a problem you were thinking about, and after you've started
   to talk to someone, you immediately actually realize what the problem is
   (and possibly also how to solve it)? That is precisely the reason: you don't
   really understand your thoughts, they're fuzzy, until you verbalize those
   thoughts.

   This looks indeed like non-programming related, and it is. But it's related
   to thinking, and programming is a highly intelectual activity. In order to
   master programming, you also have to master your thoughts (and the process
   of thinking), by definition.

2. `Connect::checkConfig()` is public. Why? What have you thought when you've
   made it public?

   It is used internally only, so it doesn't make sense to be public, does it?

3. Implementation detail: a variable in `Connect::query()` is called `$sth`.
   Name it after the *thing* it actually holds

4. Why is `Connect::query()` a method of `Connect` at all? The ambiguity
   introduced by issue 1. above may have a word here - it's not obvious what
   kind of object `Connect` represents, but I suspect that `query()` should not
   be in this class

5. Implementation detail: look at lines 77 and 126. Does it ring a bell?

   Think about the execution flow and those negations.

6. Why are the member variables of `Connect` all `public`? Think about
   **object encapsulation**

   There is no rule of thumb, and even if there was, it would had been
   dangerous. You should judge on a case by case basis. For instance,
   `Connect::checkConfig()` is used only internally. The same could go for
   `$config` and `$requiredConfig`, perhaps others as well, cannot be said now,
   as it's not clear what type of objects the class `Connect` actually
   represents - since there's no class-level docstring and the name of the
   class is also poorly chosen.

7. Implementation detail: `$requiredConfig` and lines 81-91 - code duplication.
   It's basically the same problem as in commit
   [73523f1b5966d660d6cf246aece6ff614f5154f5](https://bitbucket.org/oaass/cuckoo/commits/73523f1b5966d660d6cf246aece6ff614f5154f5).

   You could `call_user_func()` to create `$this->pdo`, and `unset()` in a loop.

8. You've mixed tabs and spaces. Use only spaces.

9. I like the overall state of the documentation - it's decent, especially for
   a project which has just started. But there are places where you document
   too much implementation details. Personally, I think that implementations
   (the body of a function/method) smaller than 30-50 LOCs should not be
   documented, except when they do extremely difficult stuff (like
   a "complicated" or "interesting" algorithm or something similar). The same
   could go for larger methods (but perhaps not larger than 50-100 LOCs
   - a function should generally fit into one screen).

   The comments in an implementation are really the comments inside the
   implementation. The other comments, the docblocks, do not document the
   implementation, but the architecture.

10. I also like the fact that you've moved the code into its own namespace,
    making it PSR-compatible. You should also move the test unit to
    a namespace, like `CuckooTest`.

    This is done by simply sticking `namespace CuckooTest` at the top of each
    test, of course. It's also clean to keep the tests outside of the
    implementation (the `src/` directory).


Commit [5de15950ac24f65e9aaa0ddb9abd36a0bba46869](https://bitbucket.org/oaass/cuckoo/commits/5de15950ac24f65e9aaa0ddb9abd36a0bba46869)
------

The previous commit contained a bug which made the whole package collapse. This was
caused by me not being careful when I was refactoring the initial code to fit with the
suggestions given in the peer-review.

This would have been prevented if I had used proper unit testing. So from this point on
I am also providing unit tests for every package. These can be found inside the tests/
folder.

Another lesson learned :)

Peer-review [5de15950ac24f65e9aaa0ddb9abd36a0bba46869](https://bitbucket.org/oaass/cuckoo/commits/5de15950ac24f65e9aaa0ddb9abd36a0bba46869)
------

What I miss here is

1. Documentation on how to run the test units. If necessary, also provide the
   configuration file.

   You already need to document these things, because the tests are part of an
   "executable unit" which the user (programmer) will need to interact with.

   Have you actually run the unit test? (I think you have, but I'll leave this
   here for the reader).

