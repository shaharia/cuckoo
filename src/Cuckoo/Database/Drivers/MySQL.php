<?php

/**
 * @package     Cuckoo\Database\Drivers
 */
namespace   Cuckoo\Database\Drivers;

/**
 * @uses        PDO
 */
use \PDO;

/**
 * MySQL package
 */
class MySQL
{

	/**
	 * Establish a connection using PDO
	 *
	 * @param	string	$hostname
	 * @param   string  $username
	 * @param   string  $password
	 * @param   string  $database
	 * @param	array	$additional
	 *
	 * @return  PDO
	 */
    public function connect($hostname, $username, $password, $database, array $additional = array())
    {
        $dsn = "mysql:dbname={$database};host={$hostname}";
        $dsn .= (array_key_exists('port', $additional)) ? ";port={$additional['port']}" : '';

        // Instantiate a new instance of PDO
        $pdo = new PDO($dsn, $username, $password);

        if (array_key_exists('charset', $additional)) {
            $pdo->query("SET NAMES '{$additional['charset']}'");
        }

        return $pdo;
    }
}