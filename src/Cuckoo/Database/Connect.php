<?php

/**
 * @package     Cuckoo\Database
 */
namespace Cuckoo\Database;

/**
 * @uses        UnexpectedValueException
 * @uses        BadFunctionCallException
 */
use \UnexpectedValueException;
use \BadFunctionCallException;

/**
 * Database class
 */
class Connect
{
    /**
     * Raw instance of PDO
     *
     * @var     null|PDO
     */
    public $pdo = null;

	/**
	 * Raw instance of PDOStatement
	 *
	 * @var		null|PDOStatement
	 */
	public $statement = null;

    /**
     * Associative array with database related configurations
     *
     * @var     array
     */
    public $config = array();

    /**
     * Array of required configuration settings
     *
     * @var     array
     */
    public $requiredConfig = array(
        'hostname',
        'username',
        'password',
        'database'
    );

    /**
     * Error message
     *
     * @var     null|string
     */
    public $errorMessage = null;

    /**
     * Database constructor
     *
     * @param   array   $config
     * @param   object  $driver
     *
     * @throws  UnexpectedValueException
	 * @throws	BadFunctionCallException
     */
    public function __construct(array $config, $driver)
    {
        // Make sure that the driver is an object
        if (!is_object($driver)) {
            $type = gettype($driver);
            throw new UnexpectedValueException('Expected $driver to be an object, ' . $type . ' was given.');
        }

        if (!$this->checkConfig($config)) {
            throw new BadFunctionCallException($this->errorMessage);
        }

		// Extract required settings
		$hostname = $config['hostname'];
		$username = $config['username'];
		$password = $config['password'];
		$database = $config['database'];

		// Remove extracted values from the array
		unset($config['hostname']);
		unset($config['username']);
		unset($config['password']);
		unset($config['database']);

		// Connect to the database
        $this->pdo = $driver->connect($hostname, $username, $password, $database, $config);
        $this->config = $config;
    }

    /**
     * Check configuration array
     *
     * This will locate any missing configuration settings that is
     * required for the package to work properly
     *
     * @param   array   $config
     *
     * @throws  BadFunctionCallException
     * @return  boolean
     */
    public function checkConfig(array $config)
    {
        $errors = array();

        // Look for missing required configuration settings
        $errors = array_diff($this->requiredConfig, array_flip($config));

        // Return error message if errors were found
        if (!empty($errors)) {
            if (sizeof($errors) == 1) {
                $this->errorMessage = 'Missing required configuration settings. No ' . join('', $errors) . ' has been configured.';
            } else {
                $lastError = array_pop($errors);
                $this->errorMessage = sprintf('Missing required configuration settings. No %s or %s has been configured.', join(', ', $errors), $lastError);
            }
        }

        return (!is_null($this->errorMessage)) ? false : true;
    }

    /**
     * Execute query statement
     *
     * @param   string  $query  The query statement
     * @param   array   $bind   Values to bind to the query if prepared statement has been used
     *
     * @return  PDOStatement
     */
    public function query($query, $bind = array())
    {
        $this->sth = $this->pdo->prepare($query);
        $this->sth->execute($bind);
        return $this->sth;
    }
}